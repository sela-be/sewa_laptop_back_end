package com.data.sewalaptop.service.implementation;

import java.util.List;

import com.data.sewalaptop.model.Customer;

public interface Customerservice {

    List<Customer> index();

    Customer register(Customer customer);

    Customer show(Long id);

}