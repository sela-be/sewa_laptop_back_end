package com.data.sewalaptop.service;

import java.util.List;

import com.data.sewalaptop.dto.Customerdto;
import com.data.sewalaptop.model.Customer;

public interface Customerservice {

    List<Customer> index();

    Customer register(Customer customer);

    Customer show(Long id);

    Customer update(Long id, Customer customer);

    Customer delete(Long id);

    Customerdto login(Customer body) throws Exception;
}